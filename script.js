const gameContainer = document.getElementById("game");
const gameOverContainer = document.getElementById("gameOver");
const startGameConatainer = document.getElementById("startGame");
const replay = document.getElementById("replay");
const play = document.getElementById("play");
const curentTries = document.getElementById("curentTries");
const lowestTriesContainer = document.getElementById("lowestTries");
const curentTriesDiv = document.getElementById("currentScore");
const levelUp = document.getElementById("levelUp");
const level = document.getElementById("level");
const restart = document.getElementById("restart");
const confirmCantainer = document.getElementById("confirmCantainer");
const confirmYes = document.getElementById("confirmYes");
const confirmNo = document.getElementById("confirmNo");

const CARDS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "yellow",
  "coral",
  1,
  2,
  3,
  4,
  5,
  8,
  9,
  10,
  12
];

let currentLevel = +window.localStorage.getItem("level") || 1;
level.innerText = currentLevel;

function createCardArray() {
  let cardArray = CARDS.slice(0, currentLevel * 2);
  cardArray = cardArray.concat(cardArray);
  let shuffledCards = shuffle(cardArray);

  return shuffledCards;
}

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}


// this function loops over the array of cards
// it creates a new div and gives it a class with the value of the card
// it also adds an event listener for a click for each card
function createDivsForCards(cardArray) {
  for (let card of cardArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(card);

    //give it a bckground from preloaded gifs
    newDiv.style = `background : url(${gifs[6].src}); background-size :cover;`;

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

let clickCounter = 0;
let cardMatchCounter = 0;
let firstClickTracker;
let tries = 0;
let gifs = preloadGifs();

function handleCardClick(event) {
  clickCounter += 1;

  let divName = event.target.classList[0];
  // console.log(divName);
  if (isNaN(divName)) {
    event.target.style.background = divName;
  }
  else {
    event.target.style = `background : url(${gifs[+divName].src}); background-size :cover;`;

    // let gifPath = "/gifs/" + divName+ ".gif";
    // event.target.style = `background : url(${gifPath}); background-size :cover;`;
  }
  if (clickCounter === 1) {
    firstClickTracker = event.target;
  }
  else {
    tries += 1;
    if (divName === firstClickTracker.classList[0] && event.target !== firstClickTracker) {
      event.target.removeEventListener("click", handleCardClick);
      firstClickTracker.removeEventListener("click", handleCardClick);
      cardMatchCounter += 1;
      if (cardMatchCounter === currentLevel * 2) {
        showGameOver();
      }
    }
    else {
      document.body.style.pointerEvents = 'none';
      setTimeout(() => {
        firstClickTracker.style = `background : url(${gifs[6].src}); background-size :cover;`;
        event.target.style = `background : url(${gifs[6].src}); background-size :cover;`;
        document.body.style.pointerEvents = 'auto';
      }, 1000)
    }
    clickCounter = 0;
  }
  curentTries.innerText = tries;
}

function startGame() {
  restart.style.display = "block";
  level.innerText = currentLevel;
  gameContainer.innerHTML = '';

  tries = 0;
  cardMatchCounter = 0;
  curentTries.innerText = tries;
  let lowestTries = window.localStorage.getItem("currentLevel") || 0;
  lowestTriesContainer.innerText = lowestTries;
  let cards = createCardArray();
  // console.log(cards);
  createDivsForCards(cards);

  startGameConatainer.style.display = "none";
  gameContainer.style.display = "flex";
  gameOverContainer.style.display = "none";
  curentTriesDiv.style.display = "block";
}

function showGameOver() {
  restart.style.display = "none";
  gameContainer.style.display = "none";
  gameOverContainer.style.display = "flex";

  let lowestTries = window.localStorage.getItem("currentLevel") || 0;
  if(currentLevel === 8){
    levelUp.style.display = "none";
    replay.innerText = "Start Again"
    window.localStorage.setItem("level", 1);
  }
  if (tries < lowestTries || lowestTries === 0) {
    window.localStorage.setItem(currentLevel, tries);
    lowestTriesContainer.innerText = tries;
  }
}
// localStorage.clear();
play.addEventListener("click", startGame);
replay.addEventListener("click", startGame);
restart.addEventListener("click", () => {
  confirmCantainer.style.display = "flex";
});
confirmYes.addEventListener("click",() => { 
  confirmCantainer.style.display = "none";
  startGame();
});
confirmNo.addEventListener("click", () => {
  confirmCantainer.style.display = "none";
});
levelUp.addEventListener("click", () => {
  currentLevel += 1;
  window.localStorage.setItem("level", currentLevel);
  startGame();
});


// when the DOM loads

function preloadGifs(){
  let gifArray = [];
  for(let i=0; i<= 12; i++){
    let gifPath = "/gifs/" + i+ ".gif";
    let gif = new Image();
    gif.src = gifPath;
    gifArray.push(gif);
  }
  return gifArray;
}